import java.util.regex.Pattern.compile

val ENABLE_LOG = "ENABLE_LOG"
val ENABLE_CRASHLYTICS = "ENABLE_CRASHLYTICS"
val ENABLE_ANALYTICS = "ENABLE_ANALYTICS"

plugins {

    id("com.android.application")
    kotlin("android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    //id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("com.google.devtools.ksp") version "1.8.10-1.0.9"
}

android {
    namespace = "com.serhiystr.comics"
    compileSdk = 34
    signingConfigs {

    }

    defaultConfig {
        applicationId = "com.serhiystr.comic"
        minSdk = 28
        targetSdk = 34
        versionCode = System.getenv("BUILD_NUMBER")?.toIntOrNull() ?: 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        buildConfigField("String", "BASE_URL", "\"https://gateway.marvel.com/\"")
        buildConfigField("String", "PUBLIC_KEY", "\"35b025fb18c60a9bc5e96fc312f9526a\"")
        buildConfigField("String", "PRIVATE_KEY", "\"20b0368967e926e884c155c079e2ea0c3f6c04b7\"")
        buildConfigField("Boolean", ENABLE_LOG, "false")
        buildConfigField("Boolean", ENABLE_CRASHLYTICS, "true")
        buildConfigField("Boolean", ENABLE_ANALYTICS, "true")
    }
    flavorDimensions += "default"
    productFlavors {
        create("dev") {
            applicationIdSuffix = ".dev"
            versionNameSuffix = "-dev"
            resValue("string", "app_name", "Comics")
            buildConfigField("Boolean", ENABLE_LOG, "true")
            buildConfigField("Boolean", ENABLE_CRASHLYTICS, "false")
            buildConfigField("Boolean", ENABLE_ANALYTICS, "false")
        }

        create("prod") {
            applicationIdSuffix = ".prod"
            versionNameSuffix = "-prod"
            resValue("string", "app_name", "Comics")
            buildConfigField("Boolean", ENABLE_LOG, "true")
            buildConfigField("Boolean", ENABLE_CRASHLYTICS, "false")
            buildConfigField("Boolean", ENABLE_ANALYTICS, "false")
        }
    }
    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
        debug {
            // signingConfig = signingConfigs.getByName("dev")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.5"
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    lint.disable += "UnusedMaterial3ScaffoldPaddingParameter"
}

dependencies {

    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
    implementation("androidx.activity:activity-compose:1.7.2")
    implementation("androidx.core:core-splashscreen:1.0.1")

    // System UI controller for compose
    implementation("com.google.accompanist:accompanist-systemuicontroller:0.30.0")

    // Material for date picker dialogs
    implementation("com.google.android.material:material:1.8.0")


    // Collapsing toolbar
    implementation("me.onebone:toolbar-compose:2.3.5")

    // Firebase
    implementation(platform("com.google.firebase:firebase-bom:31.3.0"))
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-analytics-ktx")
    implementation("com.google.firebase:firebase-messaging-ktx")

    // DI

    val glide = "4.14.2"
    val glideTransformation = "4.3.0"
    // Image loader
    implementation("com.github.skydoves:landscapist-glide:2.1.11")
    api("com.github.bumptech.glide:glide:$glide")
    api("jp.wasabeef:glide-transformations:$glideTransformation")
    implementation("io.coil-kt:coil-compose:2.3.0")
    implementation("io.coil-kt:coil-video:2.3.0")

    // Lottie
    implementation("com.airbnb.android:lottie-compose:6.0.0")

    // Image cropper
    implementation("com.vanniktech:android-image-cropper:4.5.0")

    // Compose
    implementation("androidx.navigation:navigation-compose:2.5.3")
    implementation("com.google.accompanist:accompanist-navigation-animation:0.28.0")
    implementation("com.google.accompanist:accompanist-navigation-material:0.28.0")
    implementation("com.google.accompanist:accompanist-permissions:0.28.0")
    val composeBom = platform("androidx.compose:compose-bom:2023.04.01")
    implementation(composeBom)
    androidTestImplementation(composeBom)
    debugImplementation(composeBom)
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.animation:animation-graphics")
    implementation("androidx.compose.runtime:runtime-livedata")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.material:material")

    val roomVersion = "2.5.0"
    implementation("androidx.room:room-runtime:$roomVersion")
    annotationProcessor("androidx.room:room-compiler:$roomVersion")
    implementation("androidx.room:room-ktx:$roomVersion")
    kapt("androidx.room:room-compiler:$roomVersion")

    // Ktor
    val ktorVersion = "2.2.3"
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-android:$ktorVersion")
    implementation("io.ktor:ktor-client-logging-jvm:$ktorVersion")
    implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-serialization-gson:$ktorVersion")

    api("androidx.core:core-ktx:1.12.0")
    api("org.jetbrains.kotlinx:kotlinx-collections-immutable:0.3.5")

    api(platform("org.jetbrains.kotlin:kotlin-bom:1.7.20"))
    api("org.jetbrains.kotlin:kotlin-stdlib")

    // DI
    api("io.insert-koin:koin-android:3.4.0")
    api("io.insert-koin:koin-android-ext:3.0.2")
    implementation("io.insert-koin:koin-androidx-compose:3.4.4")

    // Gson
    api("com.google.code.gson:gson:2.10.1")

    // Data Store
    implementation("androidx.datastore:datastore-preferences:1.0.0")

    // Tests
    testImplementation("junit:junit:4.13.2")
    testImplementation(kotlin("test"))
    androidTestImplementation(kotlin("test"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    androidTestImplementation("io.insert-koin:koin-test:3.4.0")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")
}

@Suppress("MaxLineLength")
tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    val flavor = getCurrentFlavor()
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.media3.common.util.UnstableApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.material3.ExperimentalMaterial3Api"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.animation.ExperimentalAnimationApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.foundation.ExperimentalFoundationApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.ui.ExperimentalComposeUiApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.material.ExperimentalMaterialApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=me.onebone.toolbar.ExperimentalToolbarApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.ui.text.ExperimentalTextApi"
    kotlinOptions.freeCompilerArgs += "-opt-in=androidx.compose.foundation.layout.ExperimentalLayoutApi"
    if (flavor == "dev") {
        kotlinOptions.freeCompilerArgs += listOf(
            "-P",
            "plugin:androidx.compose.compiler.plugins.kotlin:reportsDestination=${project.buildDir.absolutePath}/compose_metrics"
        )
        kotlinOptions.freeCompilerArgs += listOf(
            "-P",
            "plugin:androidx.compose.compiler.plugins.kotlin:metricsDestination=${project.buildDir.absolutePath}/compose_metrics"
        )
    }
}

fun getCurrentFlavor(): String {
    val tskReqStr = gradle.startParameter.taskRequests.toString()

    val pattern =
        if (tskReqStr.contains("assemble")) {
            compile("assemble(\\w+)(Release|Debug)")
        } else if (tskReqStr.contains("bundle")) {
            compile("bundle(\\w+)(Release|Debug)")
        } else {
            compile("generate(\\w+)(Release|Debug)")
        }

    val matcher = pattern.matcher(tskReqStr)

    return if (matcher.find()) {
        matcher.group(1).toLowerCase()
    } else {
        ""
    }
}
