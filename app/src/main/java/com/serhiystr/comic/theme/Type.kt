package com.lesautomotive.bidacar.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val Typography = Typography()

val Typography.titleBold14
    get() = TextStyle(
        fontSize = 14.sp,
        lineHeight = 19.sp,
        fontWeight = FontWeight.W700
    )
val Typography.titleBold16
    get() = TextStyle(
        fontSize = 16.sp,
        lineHeight = 21.sp,
        fontWeight = FontWeight.W700
    )

val Typography.titleBold18
    get() = TextStyle(
        fontSize = 18.sp,
        lineHeight = 24.sp,
        fontWeight = FontWeight.W700
    )

val Typography.contentMedium12
    get() = TextStyle(
        fontSize = 12.sp,
        lineHeight = 17.sp,
        fontWeight = FontWeight.W500
    )

val Typography.contentMedium14
    get() = TextStyle(
        fontSize = 14.sp,
        lineHeight = 19.sp,
        fontWeight = FontWeight.W500
    )

val Typography.contentMedium16
    get() = TextStyle(
        fontSize = 16.sp,
        lineHeight = 21.sp,
        fontWeight = FontWeight.W500
    )
val Typography.buttonRegular14
    get() = TextStyle(
        fontSize = 14.sp,
        lineHeight = 29.sp,
        fontWeight = FontWeight.W400
    )

val Typography.contentRegular12
    get() = TextStyle(
        fontSize = 12.sp,
        lineHeight = 17.sp,
        fontWeight = FontWeight.W400
    )
val Typography.contentRegular14
    get() = TextStyle(
        fontSize = 14.sp,
        lineHeight = 19.sp,
        fontWeight = FontWeight.W400
    )

val Typography.contentRegular16
    get() = TextStyle(
        fontSize = 16.sp,
        lineHeight = 21.sp,
        fontWeight = FontWeight.W400
    )


val Typography.contentRegular18
    get() = TextStyle(
        fontSize = 18.sp,
        lineHeight = 24.sp,
        fontWeight = FontWeight.W400
    )
