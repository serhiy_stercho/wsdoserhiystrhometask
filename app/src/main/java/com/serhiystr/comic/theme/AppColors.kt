package com.lesautomotive.bidacar.theme

import androidx.compose.ui.graphics.Color

object AppColors {

    val colorPrimary = Color(0xFF002F3D)
    val colorAccent = Color(0xFF0172C5)
    val colorBorder = Color(0xFFD5D5D5)
    val colorTabIndicator = Color(0xFF000F1E)
    val colorTintDarkGrey = Color(0xFF656565)
    val colorSectionGrey = Color(0xFFE6E5E5)
    val colorError = Color(0xFFC50C0C)
    val colorTextDarkGrey = Color(0xFF989898)
    val colorAcceptBid = Color(0xFF076A0E)
    val colorCounterBid = Color(0xFF07076A)
    val colorDropdownBorder = Color(0xFF8E8E8E)
    val colorIconTintDark = Color(0xFF8F8F8F)

    val colorBlack50 = Color(0xFF80000000)

    object GlobalMessage {
        val colorSuccess = Color(0xFF076A0E)
        val colorError = Color(0xFF80C50C0C)
    }

    object Login {
        val textFieldBorder = Color(0xFFD5D5D5)
        val loginButtonColor = Color(0xFFD5D5D5)
    }

    object VehicleListing {
        val infoTableBorderColor = Color(0xFFD2D2D2)
        val vehicleInfoTextColor = Color(0xFF1913B2)
    }

    object Account {
        val screenBackground = Color(0xFFECECEC)
        val emailBackground = Color(0xFFDCDCDC)
    }

    object VehicleStatus {
        val dropdownBorderColor = Color(0xFF707070)
        val dropdownSeparatorColor = Color(0xFFCACACA)
    }

    object HowToVideos {
        val pageBg = Color(0xFFEBEBEB)
    }

    object GlobalLoader {
        val loaderBackground = colorPrimary
    }

    object VehicleStatusColors {
        val green = Color(0xFF3E8805)
        val yellow = Color(0xFFF9F45C)
        val red = Color(0xFFFE0000)
        val blue = Color(0xFF006FFF)
    }

    object TextFieldColors {
        val commentPlaceholderColor = Color(0xFFD2D2D2)
        val labelColorGray = Color(0xFFB8B8B8)
    }
}
