package com.lesautomotive.bidacar.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Shapes
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import com.serhiystr.comic.utils.constants.Dimens

private val LightColorScheme = lightColorScheme(
    primary = AppColors.colorPrimary,
    secondary = AppColors.colorBorder,
    tertiary = Color.White,
    background = Color.White,
    error = AppColors.colorError,
    onSurface = Color.Black,
    onPrimary = Color.White,
    surfaceVariant = AppColors.colorSectionGrey
)

@Composable
fun BidacarTheme(
    typography: androidx.compose.material3.Typography = Typography,
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colorScheme = LightColorScheme,
        typography = Typography,
        content = content,
        shapes = Shapes(
            extraSmall = RoundedCornerShape(Dimens.margin2x)
        )
    )
}
