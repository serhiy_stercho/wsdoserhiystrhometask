package com.serhiystr.comic.utils.ext


import android.content.Context

import androidx.activity.ComponentActivity


fun Context.pressBack() {
    (this as? ComponentActivity)?.onBackPressedDispatcher?.onBackPressed()
}

