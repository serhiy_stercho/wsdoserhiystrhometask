package com.serhiystr.comic.utils.constants

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

object Dimens {
    val margin = 8.dp
    val margin2dp = 2.dp
    val margin6dp = 6.dp
    val margin9dp = 9.dp
    val margin10dp = 10.dp
    val margin18dp = 18.dp
    val marginAndHalf = 12.dp
    val marginHalf = margin / 2
    val margin2x = margin * 2
    val margin2xAndHalf = 20.dp
    val margin3x = margin * 3
    val margin4x = margin * 4
    val margin4xAndHalf = margin * 4 + marginHalf
    val margin5x = margin * 5
    val margin7x = margin * 7
    val margin8x = margin * 8


    object ComicsScreen {
        val loaderIndicatorSize = 42.dp
    }

    object FavouritesScreen {
        val emptyStateIconSize = 64.dp
    }

    object SellNow {
        val vinTextFieldHeight = 36.dp
    }

    object GlobalLoader {
        val loaderBGRounding = 12.dp
        val innerPadding = 16.dp
        val strokeWidth = 3.dp
    }

    object BottomNavigationBar {
        val height = 96.dp
    }

    object BaseToolbar {
        val verticalPadding = marginAndHalf
        val height = 64.dp
        val titleSize = 20.sp
    }

    object Loader {
        val imageLoader = 20.dp
    }

    object VehicleCard {
        val shadowElev = 6.dp
        val corners = 6.dp
    }

    object ColorActionButton {
        val corners = 8.dp
    }
}
