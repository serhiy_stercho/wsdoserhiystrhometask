package com.serhiystr.comic.utils

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavController

typealias VoidCallback = () -> Unit
typealias VoidCallbackParam<T> = (T) -> Unit
typealias VoidCallbackComposable = @Composable () -> Unit
typealias FloatProvider = () -> Float
typealias ColorProvider = () -> Color
typealias BooleanProvider = () -> Boolean
typealias StringProvider = () -> String
typealias StringNullableProvider = () -> String?
typealias NavControllerProvider = () -> NavController
typealias OnTextChanged = (String) -> Unit