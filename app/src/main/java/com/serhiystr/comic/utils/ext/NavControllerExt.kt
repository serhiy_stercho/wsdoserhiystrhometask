package com.serhiystr.comic.utils.ext

import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.NavOptionsBuilder


fun NavOptionsBuilder.dropStack(navController: NavController) {
    popUpTo(navController.backQueue.first().destination.id) {
        inclusive = true
    }
}

fun ArrayDeque<NavBackStackEntry>.containRoute(value: String) =
    any { it.destination.route?.contains(value) == true }