package com.lesautomotive.bidacar.utils.ext

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn

@Suppress("MagicNumber")
val SharingStarted.Companion.whileSubscribed get() = SharingStarted.WhileSubscribed(5000)

fun <T> Flow<T>.stateIn(
    scope: CoroutineScope,
    initialValue: T
) = this@stateIn.stateIn(
    scope,
    SharingStarted.whileSubscribed,
    initialValue
)

fun <T> Flow<T?>.toStateNullable(
    scope: CoroutineScope,
    initialValue: T? = null
) = stateIn(
    scope,
    SharingStarted.whileSubscribed,
    initialValue
)
