package com.serhiystr.comic.utils.constants

import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

object Durations {

    val globalMessage = 2.5.seconds

    object WelcomeScreen {
        val duration = 2.seconds
        val rotation = 1.seconds
        val enter = 1.seconds
    }

    object Animation {
        val animationEnterScreenObject = 0.3.seconds
        val animationEnterLoginScreenObject = 1.2.seconds
        val onboardingEnter = 0.5.seconds
        val pagerIndicator = 0.3.seconds
        val splash = 0.3.seconds
        val spring = 200.milliseconds
        const val default = 300
        const val splashAnimation = 200

        object Placeholder {
            val enter = 300.milliseconds
            val enterDelay = 100.milliseconds
            val enterGeneral = enter
            const val exit = 300
        }

        object ShowOnStart {
            val delay = 100.milliseconds
        }
    }

    object Notification {
        val duration = 3.seconds
    }

    object Posts {
        const val postTimelineUpdateMilis = 100L
    }

    object Chat {
        val transition = 300.milliseconds
    }
}