package com.serhiystr.comic.remote.exceptions

class InternetConnectionException : Exception("No internet connection")