package com.serhiystr.comic.remote.exceptions

class UnauthorizedException : Exception("User is not authorized")