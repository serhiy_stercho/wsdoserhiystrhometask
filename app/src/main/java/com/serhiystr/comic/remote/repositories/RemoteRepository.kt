package com.serhiystr.comic.remote.repositories

import com.serhiystr.comic.remote.extentions.getResult
import com.serhiystr.comic.remote.data.response.ComicsResponse
import com.serhiystr.comic.utils.ext.generateMD5Hash
import com.serhiystr.comics.BuildConfig
import io.ktor.client.HttpClient
import io.ktor.client.request.parameter
import io.ktor.util.AttributeKey

interface RemoteRepository {
    suspend fun getComics(startYear: Int): Result<ComicsResponse>
}

internal class RemoteRepositoryImpl(
    private val httpClient: HttpClient
): RemoteRepository {

    override suspend fun getComics(startYear: Int): Result<ComicsResponse> {
        return httpClient.getResult("v1/public/comics") {
            parameter("startYear", startYear)

            //Hardcoded for test application need to use Query Interceptor!!!
            val timestamp = System.currentTimeMillis()
            parameter("ts", timestamp)
            parameter("apikey", BuildConfig.PUBLIC_KEY)
            parameter("hash",
                "$timestamp${BuildConfig.PRIVATE_KEY}${BuildConfig.PUBLIC_KEY}".generateMD5Hash()
            )
            /////////////

            //Hardcoded for test application!!!
            parameter("limit", 15)
            parameter("formatType", "comic")
            parameter("format", "comic")
            ////////////////
        }
    }

}