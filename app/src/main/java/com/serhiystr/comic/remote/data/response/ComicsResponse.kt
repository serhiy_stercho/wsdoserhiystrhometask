package com.serhiystr.comic.remote.data.response

data class ComicsResponse(
    val data: Data
) {

    data class Data(
        val results: List<ComicsResultResponse>
    )

    data class ComicsResultResponse(
        val id: Int,
        val digitalId: Int,
        val title: String,
        val variantDescription: String,
        val description: String,
        val thumbnail: ThumbnailResponse,
        val prices: List<PriceResponse>
    )

    data class ThumbnailResponse(
        val path: String,
        val extension: String,
    )

    data class PriceResponse(
        val type: String,
        val price: Float
    )
}