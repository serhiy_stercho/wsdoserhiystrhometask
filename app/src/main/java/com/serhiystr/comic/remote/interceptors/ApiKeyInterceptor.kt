package com.serhiystr.comic.remote.interceptors

import com.serhiystr.comics.BuildConfig
import com.serhiystr.comic.utils.ext.generateMD5Hash
import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.util.AttributeKey

internal fun HttpClient.setupApiKeyInterceptor() {
    plugin(HttpSend).intercept { request ->
        val timestamp = System.currentTimeMillis()
        request.attributes.put(AttributeKey("ts"), timestamp)
        request.attributes.put(AttributeKey("apikey"), BuildConfig.PUBLIC_KEY)
        request.attributes.put(
            AttributeKey("hash"),
            "$timestamp${BuildConfig.PRIVATE_KEY}${BuildConfig.PUBLIC_KEY}".generateMD5Hash()
        )

        execute(request)
    }
}