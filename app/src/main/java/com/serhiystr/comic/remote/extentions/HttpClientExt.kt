package com.serhiystr.comic.remote.extentions

import android.util.Log
import com.serhiystr.comic.remote.exceptions.InternetConnectionException
import com.serhiystr.comic.remote.exceptions.TimeoutException
import com.serhiystr.comic.remote.exceptions.UnknownException
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.network.sockets.ConnectTimeoutException
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.delete
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.client.request.get
import io.ktor.client.request.patch
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData


internal suspend inline fun <reified T> HttpClient.getResult(
    url: String,
    block: HttpRequestBuilder.() -> Unit = {}
): Result<T> {
    return try {
        val response = get(url, block)
        response.processResult()
    } catch (e: InternetConnectionException) {
        Result.failure(e)
    } catch (e: ConnectTimeoutException) {
        Result.failure(TimeoutException())
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

internal suspend inline fun <reified T> HttpClient.postResult(
    url: String,
    block: HttpRequestBuilder.() -> Unit = {}
): Result<T> {
    return try {
        val response = post(url, block)
        response.processResult()
    } catch (e: InternetConnectionException) {
        Result.failure(e)
    } catch (e: ConnectTimeoutException) {
        Result.failure(TimeoutException())
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

internal suspend inline fun <reified T> HttpClient.patchResult(
    url: String,
    block: HttpRequestBuilder.() -> Unit = {}
): Result<T> {
    return try {
        val response = patch(url, block)
        response.processResult()
    } catch (e: InternetConnectionException) {
        Result.failure(e)
    } catch (e: ConnectTimeoutException) {
        Result.failure(TimeoutException())
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

internal suspend inline fun <reified T> HttpClient.postMultipartResult(
    url: String,
    partData: List<PartData>,
    block: HttpRequestBuilder.() -> Unit = {}
): Result<T> {
    return try {
        val response = submitFormWithBinaryData(url, partData, block)
        response.processResult()
    } catch (e: InternetConnectionException) {
        Result.failure(e)
    } catch (e: ConnectTimeoutException) {
        Result.failure(TimeoutException())
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

internal suspend inline fun <reified T> HttpClient.deleteResult(
    url: String,
    block: HttpRequestBuilder.() -> Unit = {}
): Result<T> {
    return try {
        val response = delete(url, block)
        response.processResult()
    } catch (e: InternetConnectionException) {
        Result.failure(e)
    } catch (e: ConnectTimeoutException) {
        Result.failure(TimeoutException())
    } catch (ex: Exception) {
        Result.failure(ex)
    }
}

private suspend inline fun <reified R> HttpResponse.processResult(): Result<R> {
    val statusCode = status
    if (statusCode != HttpStatusCode.OK) {
        Log.w("Tag","Unknown error: $statusCode")
        return Result.failure(UnknownException())
    }

    val apiResponse = body<R>()

    return Result.success(apiResponse)
}

private suspend inline fun <reified R> HttpResponse.processResultApiResponse(): Result<R> {
    val statusCode = status
    if (statusCode != HttpStatusCode.OK) {
        Log.w("Tag","Unknown error: $statusCode")
        return Result.failure(UnknownException())
    }

    val apiResponse = body<R>()

    return Result.success(apiResponse)
}
