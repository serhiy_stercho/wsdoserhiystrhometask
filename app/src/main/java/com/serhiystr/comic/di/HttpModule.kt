package com.serhiystr.comic.di


import com.serhiystr.comic.remote.interceptors.setupApiKeyInterceptor
import com.serhiystr.comics.BuildConfig
import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.plugins.DefaultRequest
import io.ktor.client.plugins.HttpResponseValidator
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.plugins.logging.LogLevel
import io.ktor.client.plugins.logging.Logger
import io.ktor.client.plugins.logging.Logging
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import io.ktor.serialization.gson.gson
import org.koin.dsl.module

import kotlin.time.Duration.Companion.seconds

private val connectionTimeout = 20.seconds

val httpModule = module {
    single {

        HttpClient(Android) {
            defaultRequest {
                url(BuildConfig.BASE_URL)
            }
            engine {
                threadsCount = 20
            }
            install(HttpTimeout) {
                this.connectTimeoutMillis = connectionTimeout.inWholeMilliseconds
            }
            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {

                    }
                }
                level = LogLevel.BODY
            }
            install(ContentNegotiation) {
                gson()
            }
            install(DefaultRequest) {
                header(HttpHeaders.ContentType, ContentType.Application.Json)
            }
            HttpResponseValidator {
                handleResponseExceptionWithRequest { cause, _ ->
                    val clientException = cause as? ClientRequestException
                        ?: return@handleResponseExceptionWithRequest
                    val exceptionResponse = clientException.response
                    if (exceptionResponse.status == HttpStatusCode.Unauthorized) {
                        // authorizationRepository.notAuthorized()
                    }
                }
            }
        }.apply {
            setupApiKeyInterceptor()
        }
    }
}
