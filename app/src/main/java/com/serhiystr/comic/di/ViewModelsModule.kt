package com.serhiystr.comic.di

import com.serhiystr.comic.presentation.app.AppViewModel
import com.serhiystr.comic.presentation.screens.comics.ComicsViewModel
import com.serhiystr.comic.presentation.screens.favourites.FavouritesViewModel
import com.serhiystr.comic.presentation.screens.main.MainScreenViewModel
import com.serhiystr.comic.presentation.screens.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {

    viewModel {
        SplashViewModel()
    }

    viewModel {
        AppViewModel()
    }

    viewModel {
        MainScreenViewModel(get())
    }

    viewModel {
        ComicsViewModel(get(), get(), get())
    }

    viewModel {
        FavouritesViewModel(get(), get())
    }
}
