package com.serhiystr.comic.di

import com.serhiystr.comic.domain.usecases.local.AddToFavourites
import com.serhiystr.comic.domain.usecases.local.AddToFavouritesImpl
import com.serhiystr.comic.domain.usecases.local.GetAllFavourites
import com.serhiystr.comic.domain.usecases.local.GetAllFavouritesAsFlow
import com.serhiystr.comic.domain.usecases.local.GetAllFavouritesAsFlowImpl
import com.serhiystr.comic.domain.usecases.local.GetAllFavouritesImpl
import com.serhiystr.comic.domain.usecases.local.RemoveFavourite
import com.serhiystr.comic.domain.usecases.local.RemoveFavouriteImpl
import com.serhiystr.comic.domain.usecases.remote.GetComicsForYear
import com.serhiystr.comic.domain.usecases.remote.GetComicsForYearImpl
import org.koin.dsl.module

val useCaseModule = module {

    factory<GetComicsForYear> { GetComicsForYearImpl(get(), get()) }
    factory<AddToFavourites> { AddToFavouritesImpl(get()) }
    factory<RemoveFavourite> { RemoveFavouriteImpl(get()) }
    factory<GetAllFavourites> { GetAllFavouritesImpl(get()) }
    factory<GetAllFavouritesAsFlow> { GetAllFavouritesAsFlowImpl(get()) }
}
