package com.serhiystr.comic.di


import androidx.room.Room
import com.serhiystr.comic.data.local.database.AppDataBase
import com.serhiystr.comic.data.local.database.AppDataBaseImpl
import org.koin.dsl.module


val dataBaseModule = module {

    single<AppDataBase> {
        Room.databaseBuilder(get(), AppDataBaseImpl::class.java, "primary_db.db")
            .fallbackToDestructiveMigration()
            .build()
    }
}
