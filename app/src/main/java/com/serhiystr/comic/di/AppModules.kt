package com.serhiystr.comic.di


val appModules = listOf(
    dataBaseModule,
    httpModule,
    viewModelsModule,
    useCaseModule,
    repositoriesModule,
)
