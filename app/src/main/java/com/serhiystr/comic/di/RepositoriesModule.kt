package com.serhiystr.comic.di


import com.serhiystr.comic.remote.repositories.RemoteRepository
import com.serhiystr.comic.remote.repositories.RemoteRepositoryImpl
import org.koin.dsl.module

val repositoriesModule = module {

    single<RemoteRepository> {
        RemoteRepositoryImpl(get())
    }
}
