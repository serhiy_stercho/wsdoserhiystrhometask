package com.serhiystr.comic.domain.usecases.local

import com.serhiystr.comic.data.local.database.AppDataBase
import com.serhiystr.comic.domain.converters.toDomainModel
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlin.coroutines.CoroutineContext

fun interface GetAllFavouritesAsFlow {
    operator fun invoke(): Flow<List<ComicsDomainModel>>
}

class GetAllFavouritesAsFlowImpl(
    private val database: AppDataBase,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
) : GetAllFavouritesAsFlow {
    override fun invoke(): Flow<List<ComicsDomainModel>> = database
        .getAllFavouritesAsFlow()
        .map { comics -> comics.map { it.toDomainModel() } }

}