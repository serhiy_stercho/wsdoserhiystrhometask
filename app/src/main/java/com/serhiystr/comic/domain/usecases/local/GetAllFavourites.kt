package com.serhiystr.comic.domain.usecases.local

import com.serhiystr.comic.data.local.database.AppDataBase
import com.serhiystr.comic.domain.converters.toDomainModel
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext


fun interface GetAllFavourites {
    suspend operator fun invoke(): List<ComicsDomainModel>
}

class GetAllFavouritesImpl(
    private val database: AppDataBase,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
) : GetAllFavourites {
    override suspend operator fun invoke(): List<ComicsDomainModel> =
        withContext(coroutineContext) {
            return@withContext database.getAllFavourites().map { it.toDomainModel() }
        }
}