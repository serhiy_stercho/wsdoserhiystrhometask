package com.serhiystr.comic.domain.usecases.local

import com.serhiystr.comic.data.local.database.AppDataBase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

fun interface RemoveFavourite {
    suspend operator fun invoke(comicsId: Int)
}

class RemoveFavouriteImpl(
    private val database: AppDataBase,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): RemoveFavourite {
    override suspend fun invoke(comicsId: Int) = withContext(coroutineContext) {
        database.deleteFavourite(comicsId)
    }

}