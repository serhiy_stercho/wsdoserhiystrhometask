package com.serhiystr.cosmicgate.domain.data

data class ComicsDomainModel(
    val id: Int,
    val digitalId: Int,
    val title: String,
    val variantDescription: String,
    val description: String,
    val thumbnail: Thumbnail,
    val price: Float,
    var isFavourite: Boolean = false
) {
    data class Thumbnail(
        val path: String,
        val extension: String,
    )


}