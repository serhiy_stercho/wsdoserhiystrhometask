package com.serhiystr.comic.domain.usecases.local

import com.serhiystr.comic.data.local.database.AppDataBase
import com.serhiystr.comic.data.local.database.entity.ComicsEntity
import com.serhiystr.comic.domain.converters.toLocalModel
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

fun interface AddToFavourites {
    suspend operator fun invoke(comics: ComicsDomainModel)
}

class AddToFavouritesImpl(
    private val database: AppDataBase,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): AddToFavourites {
    override suspend fun invoke(comics: ComicsDomainModel) = withContext(coroutineContext) {
        database.insertFavourite(comics.toLocalModel())
    }

}