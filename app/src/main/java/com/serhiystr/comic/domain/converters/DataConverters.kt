package com.serhiystr.comic.domain.converters

import com.serhiystr.comic.data.local.database.entity.ComicsEntity
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel
import com.serhiystr.comic.remote.data.response.ComicsResponse


fun ComicsResponse.ComicsResultResponse.toDomainModel() = ComicsDomainModel(
    id = id,
    digitalId = digitalId,
    title = title,
    variantDescription = variantDescription,
    description = description,
    thumbnail = thumbnail.toDomainModel(),
    price = prices.first().price
)

fun ComicsResponse.ThumbnailResponse.toDomainModel(): ComicsDomainModel.Thumbnail = ComicsDomainModel.Thumbnail(
    path = path,
    extension = extension
)

fun ComicsEntity.toDomainModel() = ComicsDomainModel(
    id = id,
    digitalId = digitalId,
    title = title,
    variantDescription = variantDescription,
    description = description,
    thumbnail = ComicsDomainModel.Thumbnail(
        path = thumbnail,
        extension = ""
    ),
    price = price.toFloat(),
    isFavourite = isFavourite
)



fun ComicsDomainModel.toLocalModel() = ComicsEntity(
    id = id,
    digitalId = digitalId,
    title = title,
    variantDescription = variantDescription,
    description = description,
    thumbnail = "${thumbnail.path}.${thumbnail.extension}",
    price = price.toString(),
    isFavourite = true
)
