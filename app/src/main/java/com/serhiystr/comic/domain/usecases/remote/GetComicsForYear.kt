package com.serhiystr.comic.domain.usecases.remote

import com.serhiystr.comic.data.local.database.AppDataBase
import com.serhiystr.comic.domain.converters.toDomainModel
import com.serhiystr.comic.domain.usecases.local.GetAllFavourites
import com.serhiystr.comic.remote.repositories.RemoteRepository
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

fun interface GetComicsForYear {
    suspend operator fun invoke(startYear: Int): Result<List<ComicsDomainModel>>
}

class GetComicsForYearImpl(
    private val remoteRepository: RemoteRepository,
    private val getAllFavourites: GetAllFavourites,
    private val coroutineContext: CoroutineContext = Dispatchers.IO
): GetComicsForYear {
    override suspend fun invoke(startYear: Int): Result<List<ComicsDomainModel>> = withContext(coroutineContext) {
        val comicsResponse = remoteRepository.getComics(startYear)

        if(comicsResponse.isFailure) {
            val exception = comicsResponse.exceptionOrNull() ?: Throwable("Could not obtain comics list")
            return@withContext Result.failure(exception)
        }

        val favouriteComics = getAllFavourites()
        val remoteComics = comicsResponse.getOrThrow().data.results.map { it.toDomainModel() }
        remoteComics.forEach { remoteComics ->
            remoteComics.isFavourite = favouriteComics.any { it.id == remoteComics.id }
        }

        return@withContext Result.success(remoteComics)
    }


}