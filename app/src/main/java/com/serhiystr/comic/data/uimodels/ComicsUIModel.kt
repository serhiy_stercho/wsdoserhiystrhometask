package com.serhiystr.comic.data.uimodels

data class ComicsUIModel(
    val id: Int,
    val digitalId: Int,
    val title: String,
    val variantDescription: String,
    val description: String,
    val thumbnail: Thumbnail,
    val price: Float,
    var isFavourite: Boolean = false
) {

    val thumb get() = "${thumbnail.path}${if(thumbnail.extension.isEmpty()) "" else String.format("%s%s",".",thumbnail.extension)}"
    data class Thumbnail(
        val path: String,
        val extension: String,
    )

    data class Price(
        val type: String,
        val price: Float
    )



}