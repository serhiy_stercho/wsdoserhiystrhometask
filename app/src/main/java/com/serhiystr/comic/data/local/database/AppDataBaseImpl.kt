package com.serhiystr.comic.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.serhiystr.comic.data.local.database.dao.ComicsDao
import com.serhiystr.comic.data.local.database.entity.ComicsEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

@Database(
    entities = [
        ComicsEntity::class,
    ],
    version = 2
)
internal abstract class AppDataBaseImpl : AppDataBase, RoomDatabase() {

    abstract val comicsDao: ComicsDao

    override fun insertFavourite(comicsEntity: ComicsEntity) {
        comicsDao.insertFavourite(comicsEntity)
    }

    override fun deleteFavourite(comicsId: Int) {
        comicsDao.deleteFavourite(comicsId)
    }

    override fun getAllFavouritesAsFlow(): Flow<List<ComicsEntity>> =
        comicsDao.getAllFavouritesAsFlow()

    override fun getAllFavourites(): List<ComicsEntity> = comicsDao.getAllFavourites()
}