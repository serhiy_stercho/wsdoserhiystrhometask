package com.serhiystr.comic.data.local.database


import com.serhiystr.comic.data.local.database.dao.ComicsDao
import com.serhiystr.comic.data.local.database.entity.ComicsEntity
import kotlinx.coroutines.flow.Flow

interface AppDataBase {

    fun insertFavourite(comicsEntity: ComicsEntity)

    fun deleteFavourite(comicsId: Int)

    fun getAllFavouritesAsFlow(): Flow<List<ComicsEntity>>
    fun getAllFavourites(): List<ComicsEntity>

}