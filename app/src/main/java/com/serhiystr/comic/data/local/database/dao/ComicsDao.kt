package com.serhiystr.comic.data.local.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.serhiystr.comic.data.local.database.entity.ComicsEntity
import kotlinx.coroutines.flow.Flow


@Dao
interface ComicsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(comics: ComicsEntity)

    @Query("DELETE FROM ComicsEntity WHERE id=:comicsId")
    fun deleteFavourite(comicsId: Int)


    @Query("SELECT * FROM ComicsEntity")
    fun getAllFavouritesAsFlow(): Flow<List<ComicsEntity>>

    @Query("SELECT * FROM ComicsEntity")
    fun getAllFavourites(): List<ComicsEntity>
}