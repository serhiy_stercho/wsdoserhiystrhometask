package com.serhiystr.comic.data.local.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity()
data class ComicsEntity(
    @PrimaryKey
    val id: Int,
    val digitalId: Int,
    val title: String,
    val variantDescription: String,
    val description: String,
    val thumbnail: String,
    val price: String,
    val isFavourite: Boolean
)