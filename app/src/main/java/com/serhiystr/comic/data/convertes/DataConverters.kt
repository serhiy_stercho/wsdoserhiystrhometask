package com.serhiystr.comic.data.convertes

import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.cosmicgate.domain.data.ComicsDomainModel

fun ComicsDomainModel.toUIModel() = ComicsUIModel(
    id = id,
    digitalId = digitalId,
    title = title,
    variantDescription = variantDescription,
    description = description,
    thumbnail = thumbnail.toUIModel(),
    price = price,
    isFavourite = isFavourite
)

fun ComicsDomainModel.Thumbnail.toUIModel(): ComicsUIModel.Thumbnail = ComicsUIModel.Thumbnail(
    path = path,
    extension = extension
)

fun ComicsUIModel.toDomainModel() = ComicsDomainModel(
    id = id,
    digitalId = digitalId,
    title = title,
    variantDescription = variantDescription,
    description = description,
    thumbnail = thumbnail.toDomainModel(),
    price = price
)

fun ComicsUIModel.Thumbnail.toDomainModel(): ComicsDomainModel.Thumbnail = ComicsDomainModel.Thumbnail(
    path = path,
    extension = extension
)



