package com.serhiystr.comic.presentation.widgets.image

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource

@Composable
fun LocalImage(
    modifier: Modifier = Modifier,
    @DrawableRes id: Int,
    contentScale: ContentScale = ContentScale.Fit
) {
    Image(
        modifier = modifier,
        painter = painterResource(id = id),
        contentDescription = null,
        contentScale = contentScale
    )
}
