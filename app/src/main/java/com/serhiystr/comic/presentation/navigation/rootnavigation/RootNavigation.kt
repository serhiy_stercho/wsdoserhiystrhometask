package com.serhiystr.comic.presentation.navigation.rootnavigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.serhiystr.comic.presentation.navigation.AppNavHost
import com.serhiystr.comic.presentation.screens.main.support.mainScreenComposable
import com.serhiystr.comic.presentation.screens.splash.support.SplashScreen
import com.serhiystr.comic.presentation.screens.splash.support.splashScreenComposable

@Composable
fun RootNavigation(
    navController: NavHostController
) {
    CompositionLocalProvider(RootNavigation provides navController) {
        AppNavHost(
            navController = navController,
            startDestination = SplashScreen.route
        ) {
            splashScreenComposable(navController)
            mainScreenComposable(navController)
        }
    }
}

val RootNavigation = compositionLocalOf<NavController?> {
    null
}
