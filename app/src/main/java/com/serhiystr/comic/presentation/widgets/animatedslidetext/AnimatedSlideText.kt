package com.serhiystr.comic.presentation.widgets.animatedslidetext

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.material3.LocalTextStyle
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import com.serhiystr.comic.presentation.widgets.autosizetext.AutoSizeText

@Suppress("MagicNumber")
@Composable
fun AnimatedSlideText(
    modifier: Modifier = Modifier,
    text: String,
    style: TextStyle = LocalTextStyle.current,
    color: Color = Color.Unspecified,
    maxLines: Int = Int.MAX_VALUE,
    overflow: TextOverflow = TextOverflow.Clip,
    isAutoSize: Boolean = false,
    textAlign: TextAlign? = null
) {
    AnimatedContent(
        modifier = modifier,
        targetState = text,
        transitionSpec = {
            (
                slideInVertically(animationSpec = tween()) { height -> -height / 4 } + fadeIn(
                    animationSpec = tween()
                ) with slideOutVertically(animationSpec = tween()) { height -> height / 4 } + fadeOut(
                    animationSpec = tween()
                )
                ).using(
                SizeTransform(clip = false)
            )
        },
        contentAlignment = Alignment.Center
    ) { text ->
        if (isAutoSize) {
            AutoSizeText(
                text = text,
                textStyle = style,
                color = color,
                maxLines = maxLines
            )
        } else {
            Text(
                text = text,
                style = style,
                color = color,
                maxLines = maxLines,
                overflow = overflow,
                textAlign = textAlign
            )
        }
    }
}
