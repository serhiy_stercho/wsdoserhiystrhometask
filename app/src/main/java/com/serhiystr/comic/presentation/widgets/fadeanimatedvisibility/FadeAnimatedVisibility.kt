package com.serhiystr.comic.presentation.widgets.fadeanimatedvisibility

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.shrinkOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.serhiystr.comic.utils.BooleanProvider
import kotlin.math.roundToInt

@Composable
fun FadeAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: Boolean,
    content: @Composable () -> Unit
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = visible,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        content()
    }
}

@Composable
fun SlideFromRightAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: Boolean,
    content: @Composable () -> Unit
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = visible,
        enter = slideInHorizontally { it },
        exit = slideOutHorizontally { it }
    ) {
        content()
    }
}

@Suppress("MagicNumber")
@Composable
fun SlideFromLeftAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: Boolean,
    offset: (fullWidth: Int) -> Int = { fullWidth: Int ->
        (-fullWidth * 1.5).roundToInt()
    },
    content: @Composable () -> Unit
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = visible,
        enter = slideInHorizontally(initialOffsetX = offset),
        exit = slideOutHorizontally(targetOffsetX = offset)
    ) {
        content()
    }
}

@Composable
fun FadeAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: BooleanProvider,
    content: @Composable () -> Unit
) {
    FadeAnimatedVisibility(
        modifier = modifier,
        visible = visible(),
        content = content
    )
}

@Composable
fun SizeVerticallyAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: Boolean,
    content: @Composable () -> Unit
) {
    AnimatedVisibility(
        modifier = modifier,
        visible = visible,
        enter = expandVertically(),
        exit = shrinkOut() + fadeOut()
    ) {
        content()
    }
}

@Composable
fun ScaleAnimatedVisibility(
    modifier: Modifier = Modifier,
    visible: Boolean,
    isAlwaysTakePlace: Boolean = false,
    content: @Composable () -> Unit
) {
    val enter = scaleIn(
        animationSpec = spring(
            stiffness = Spring.StiffnessMediumLow,
            dampingRatio = Spring.DampingRatioMediumBouncy
        )
    )
    val exit = scaleOut(
        animationSpec = spring(
            stiffness = Spring.StiffnessMediumLow,
            dampingRatio = Spring.DampingRatioNoBouncy
        )
    )
    if (isAlwaysTakePlace) {
        Box(modifier) {
            AnimatedVisibility(
                visible = visible,
                enter = enter,
                exit = exit
            ) {
                content()
            }
        }
    } else {
        AnimatedVisibility(
            modifier = modifier,
            visible = visible,
            enter = enter,
            exit = exit
        ) {
            content()
        }
    }
}
