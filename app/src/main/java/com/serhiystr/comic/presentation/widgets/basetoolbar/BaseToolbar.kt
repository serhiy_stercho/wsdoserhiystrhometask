package com.serhiystr.comic.presentation.widgets.basetoolbar

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import com.lesautomotive.bidacar.theme.AppColors
import com.serhiystr.comic.utils.constants.Dimens

@Composable
fun BaseToolbar(title: String) {
    Box(
        Modifier
            .fillMaxWidth()
            .background(AppColors.colorPrimary)
            .height(Dimens.BaseToolbar.height)
    ) {
        Text(
            text = title,
            modifier = Modifier.align(Alignment.Center),
            style = TextStyle(
                fontSize = Dimens.BaseToolbar.titleSize,
                fontWeight = FontWeight.W700
            ),
            color = Color.White
        )
    }
}
