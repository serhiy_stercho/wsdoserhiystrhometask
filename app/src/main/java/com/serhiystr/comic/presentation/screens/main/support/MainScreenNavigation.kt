package com.serhiystr.comic.presentation.screens.main.support

import androidx.compose.foundation.rememberScrollState
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.animatedComposable
import com.serhiystr.comic.presentation.screens.main.MainScreen
import com.serhiystr.comic.utils.ext.dropStack

object MainScreen {
    const val path = "main"
    val route = RouteBuilder.buildRoute(
        path
    )
}

fun NavController.navigateToMain() {
    val uri = RouteBuilder.buildDestination(
        path = MainScreen.path
    )
    navigate(uri) {
        dropStack(this@navigateToMain)
    }
}

fun NavGraphBuilder.mainScreenComposable(
    navController: NavController
) {
    animatedComposable(
        MainScreen.route
    ) { navBackStackEntry ->
        rememberScrollState()
        MainScreen(
            navController = navController
        )
    }
}
