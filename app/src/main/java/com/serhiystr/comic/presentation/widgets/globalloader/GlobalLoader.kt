package com.serhiystr.comic.presentation.widgets.globalloader

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import com.serhiystr.comic.presentation.widgets.fadeanimatedvisibility.FadeAnimatedVisibility
import com.lesautomotive.bidacar.theme.AppColors
import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens
import com.serhiystr.comic.utils.constants.Dimens.GlobalLoader.strokeWidth

@Composable
fun GlobalLoader(
    state: GlobalLoaderState,
    content: @Composable (GlobalLoaderState) -> Unit
) {
    CompositionLocalProvider(LocalGlobalLoader provides state) {
        Box(
            Modifier.fillMaxSize()
        ) {
            content(state)
            ProgressContent(
                show = state.show,
                removable = state.removable
            ) {
                state.hide()
            }
        }
    }
}

@Composable
private fun ProgressContent(
    show: Boolean,
    removable: Boolean,
    remove: VoidCallback
) {
    FadeAnimatedVisibility(
        visible = show
    ) {
        Box(
            Modifier
                .fillMaxSize()
                .background(Color.Black.copy(alpha = 0.7F))
                .clickable(
                    indication = null,
                    interactionSource = remember { MutableInteractionSource() }
                ) {
                    if (removable) {
                        remove()
                    }
                }
        ) {
            LoadContent()
        }
    }
}

@Composable
private fun Loader() {
    CircularProgressIndicator(
        color = Color.White,
        strokeWidth = strokeWidth
    )
}

@Composable
private fun LoadContent() {
    Box(modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier.align(Alignment.Center)
                .background(
                    AppColors.colorPrimary,
                    RoundedCornerShape(Dimens.GlobalLoader.loaderBGRounding)
                ).padding(Dimens.GlobalLoader.innerPadding)
        ) {
            Loader()
        }
    }
}

class GlobalLoaderState {
    var show by mutableStateOf(false)
        private set
    var removable by mutableStateOf(false)
        private set

    fun showIf(show: Boolean) {
        if (show) {
            show()
        } else {
            hide()
        }
    }

    fun show(cancelable: Boolean = false) {
        show = true
        removable = cancelable
    }

    fun hide() {
        show = false
    }
}

val LocalGlobalLoader = compositionLocalOf { GlobalLoaderState() }

@Composable
fun ListenProgress(
    progress: Boolean?,
    onProgressCatch: VoidCallback
) {
    val loader = LocalGlobalLoader.current
    LaunchedEffect(progress) {
        progress ?: return@LaunchedEffect
        onProgressCatch()
        loader.showIf(progress)
    }
}
