package com.serhiystr.comic.presentation.widgets.clickabletextfield

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.serhiystr.comic.presentation.widgets.animatedslidetext.AnimatedSlideText
import com.serhiystr.comic.presentation.widgets.autosizetext.AutoSizeText
import com.lesautomotive.bidacar.theme.AppColors
import com.lesautomotive.bidacar.theme.contentMedium16
import com.serhiystr.comic.utils.OnTextChanged
import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens
import com.serhiystr.comics.R

@Composable
fun ClickableTextField(
    modifier: Modifier = Modifier,
    value: String,
    onClick: VoidCallback,
    onValueChanged: OnTextChanged = {},
    error: String? = null,
    label: String? = null,
    placeHolder: String? = null,
    leadingIcon: (@Composable () -> Unit)? = null,
    shape: Shape = RoundedCornerShape(Dimens.marginAndHalf),
    displayTrailingIcon: Boolean = true,
    isAutoSize: Boolean = true,
    maxLines: Int = 1,
    editable: Boolean = false
) {
    Box(modifier) {
        if (editable) {
            BasicTextField(
                modifier = Modifier
                    .padding(Dimens.margin2x, 0.dp, 0.dp, 0.dp)
                    .clickable(
                        indication = null,
                        interactionSource = remember { MutableInteractionSource() }
                    ) { onClick() },
                value = value.takeIf { it.isNotEmpty() } ?: placeHolder ?: "",
                onValueChange = onValueChanged,
                readOnly = editable.not(),
                singleLine = true,
                textStyle = MaterialTheme.typography.contentMedium16,
                enabled = editable
            )
        } else {
            Row(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(
                        start = Dimens.margin2x,
                        end = Dimens.margin
                    ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                leadingIcon?.let {
                    Box {
                        it()
                    }
                    Spacer(Modifier.width(Dimens.margin))
                }
                when {
                    value.isEmpty() -> {
                        if ((placeHolder ?: "") != "") {
                            AutoSizeText(
                                modifier = Modifier
                                    .weight(1F),
                                text = placeHolder ?: "",
                                textStyle = MaterialTheme.typography.contentMedium16,
                                color = Color.Black

                            )
                        } else {
                            AutoSizeText(
                                modifier = Modifier
                                    .weight(1F),
                                text = label ?: "",
                                textStyle = MaterialTheme.typography.contentMedium16,
                                color = Color.Black
                            )
                        }
                    }

                    else -> {
                        AnimatedSlideText(
                            modifier = Modifier
                                .weight(1F),
                            text = value,
                            style = MaterialTheme.typography.contentMedium16,
                            color = Color.Black,
                            isAutoSize = isAutoSize,
                            maxLines = maxLines
                        )
                    }
                }
                if (displayTrailingIcon) {
                    Spacer(Modifier.width(Dimens.margin))
                    Icon(
                        modifier = Modifier.padding(top = Dimens.marginHalf).rotate(-90F),
                        painter = painterResource(R.drawable.ic_arrow_back),
                        contentDescription = null,
                        tint = AppColors.colorIconTintDark
                    )
                }
            }
        }
    }
}
