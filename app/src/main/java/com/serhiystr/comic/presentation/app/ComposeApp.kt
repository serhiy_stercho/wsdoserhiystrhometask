package com.serhiystr.comic.presentation.app

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.serhiystr.comic.presentation.app.containers.AppDialogs
import com.serhiystr.comic.presentation.navigation.rootnavigation.RootNavigation
import com.serhiystr.comic.presentation.widgets.globalloader.GlobalLoaderState
import com.serhiystr.comic.presentation.widgets.globalmessage.GlobalMessage
import com.lesautomotive.bidacar.theme.BidacarTheme
import org.koin.androidx.compose.getViewModel

@Composable
fun ComposeApp(
    globalLoaderState: GlobalLoaderState,
    viewModel: AppViewModel = getViewModel()
) {
    val context = LocalContext.current

    val navController = rememberAnimatedNavController()
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    BidacarTheme {
        AppDialogs(loaderState = globalLoaderState) {
            GlobalMessage {
                RootNavigation(
                    navController
                )
            }
        }
    }
}

private fun NavController.currentDestinationIs(vararg route: String): Boolean {
    route.forEach {
        if (it == currentDestination?.route) {
            return true
        }
    }
    return false
}
