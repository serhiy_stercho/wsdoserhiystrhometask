package com.serhiystr.comic.presentation.main

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowCompat
import com.serhiystr.comic.presentation.app.ComposeApp
import com.serhiystr.comic.presentation.widgets.globalloader.GlobalLoaderState

class MainActivity : AppCompatActivity() {

    private val globalLoaderState = GlobalLoaderState()

    override fun onCreate(savedInstanceState: Bundle?) {
        WindowCompat.setDecorFitsSystemWindows(window, false)

        super.onCreate(savedInstanceState)

        setContent {
            ComposeApp(globalLoaderState)
        }
    }
}
