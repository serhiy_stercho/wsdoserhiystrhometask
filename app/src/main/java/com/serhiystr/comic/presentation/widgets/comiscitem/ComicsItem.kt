package com.serhiystr.comic.presentation.widgets.comiscitem

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.zIndex
import com.lesautomotive.bidacar.theme.AppColors
import com.lesautomotive.bidacar.theme.contentMedium16
import com.lesautomotive.bidacar.theme.contentRegular12
import com.serhiystr.comic.utils.constants.Dimens
import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.comic.presentation.widgets.image.RemoteImage
import com.serhiystr.comics.R

typealias OnComicFavouriteClicked = (ComicsUIModel) -> Unit

@Composable
fun ComicsItem(
    comics: ComicsUIModel,
    onComicFavouriteClicked: OnComicFavouriteClicked
) {
    val favouriteIcon =
        if (comics.isFavourite)
            R.drawable.ic_star_filed
        else R.drawable.ic_star_outline


    Row(


        Modifier
            .fillMaxWidth()
            .padding(Dimens.margin6dp)
            .shadow(
                Dimens.VehicleCard.shadowElev,
                RoundedCornerShape(Dimens.VehicleCard.corners),
                true
            )
            .zIndex(0.5F)
            .background(
                color = Color.White,
                shape = RoundedCornerShape(Dimens.VehicleCard.corners)
            )
            .clip(RoundedCornerShape(Dimens.VehicleCard.corners))
            .padding(Dimens.margin6dp)
    ) {
        Column(Modifier.fillMaxWidth(0.3F)) {
            RemoteImage(
                modifier = Modifier.aspectRatio(9 / 16F),
                url = comics.thumb
            )
            Spacer(modifier = Modifier.height(Dimens.margin))
            Text(
                text = comics.price.toString(),
                modifier = Modifier.fillMaxWidth(),
                style = MaterialTheme.typography.contentMedium16,
                color = AppColors.colorTextDarkGrey
            )
            Spacer(modifier = Modifier.height(Dimens.margin))
            Image(
                painter = painterResource(id = favouriteIcon),
                contentDescription = null,
                modifier = Modifier
                    .clickable { onComicFavouriteClicked(comics) }
            )
        }
        Spacer(modifier = Modifier.width(Dimens.margin))
        Column(Modifier.fillMaxWidth(0.7F)) {
            Text(
                text = comics.title,
                modifier = Modifier.fillMaxWidth(),
                style = MaterialTheme.typography.contentMedium16,
                color = Color.Black
            )
            Spacer(modifier = Modifier.height(Dimens.marginHalf))
            Text(
                text = comics.description,
                modifier = Modifier.fillMaxWidth(),
                style = MaterialTheme.typography.contentRegular12,
                color = AppColors.colorTextDarkGrey
            )
        }
    }
}