package com.serhiystr.comic.presentation.widgets.globalmessage

import androidx.compose.ui.graphics.Color
import com.lesautomotive.bidacar.theme.AppColors

enum class GlobalMessageStyle(
    val backgroundColor: Color
) {
    ERROR(
        backgroundColor = AppColors.GlobalMessage.colorError
    ),
    SUCCESS(
        backgroundColor = AppColors.GlobalMessage.colorSuccess
    ),
}
