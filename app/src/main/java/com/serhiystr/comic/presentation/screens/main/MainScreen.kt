package com.serhiystr.comic.presentation.screens.main

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.foundation.layout.navigationBars
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.serhiystr.comic.presentation.navigation.mainnavigation.MainNavigation
import com.serhiystr.comic.presentation.navigation.rootnavigation.RootNavigation
import com.serhiystr.comic.presentation.screens.main.support.MainScreenScope
import com.serhiystr.comic.presentation.screens.main.ui.BottomBar
import com.serhiystr.comic.presentation.screens.main.ui.OnBottomNavOptionSelected
import org.koin.androidx.compose.getViewModel

@Composable
fun MainScreen(
    navController: NavController,
    viewModel: MainScreenViewModel = getViewModel()
) {
    val context = LocalContext.current
    val rootNavController = RootNavigation.current
    val favouritesCount by viewModel.favouritesSize.collectAsState()

    val bottomNavController = rememberNavController()

    MainContent(
        bottomBarNavController = bottomNavController,
        onBottomNavOptionSelected = {
        },
        favouritesCount = favouritesCount
    )
}

@Composable
private fun MainContent(
    bottomBarNavController: NavHostController,
    onBottomNavOptionSelected: OnBottomNavOptionSelected,
    favouritesCount: Int
) {
    Scaffold(
        contentWindowInsets = WindowInsets.navigationBars,
        bottomBar = {
            MainScreenScope.BottomBar(
                navController = bottomBarNavController,
                onBottomNavOptionSelected = onBottomNavOptionSelected,
                favouritesCount = favouritesCount
            )
        }
    ) {
        Box(modifier = Modifier.padding(it)) {
            MainNavigation(navController = bottomBarNavController)
        }
    }
}
