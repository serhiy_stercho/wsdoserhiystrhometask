package com.serhiystr.comic.presentation.navigation.comicsnavigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.serhiystr.comic.presentation.navigation.AppNavHost
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.screens.comics.support.ComicsScreen
import com.serhiystr.comic.presentation.screens.comics.support.comicsScreenComposable



@Composable
fun ComicsNavigation(
    modifier: Modifier = Modifier,
    navController: NavHostController
) {
    AppNavHost(
        modifier = modifier,
        navController = navController,
        startDestination = RouteBuilder.buildRoute(ComicsScreen.route)
    ) {
        comicsScreenComposable(navController)
    }
}
