package com.serhiystr.comic.presentation.screens.main.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Badge
import androidx.compose.material3.BadgedBox
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.compose.currentBackStackEntryAsState
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.screens.main.support.MainScreenScope
import com.serhiystr.comic.presentation.screens.main.support.MainScreenTabs
import com.lesautomotive.bidacar.theme.AppColors
import com.serhiystr.comic.utils.constants.Dimens

typealias OnBottomNavOptionSelected = (MainScreenTabs) -> Unit

@Composable
fun MainScreenScope.BottomBar(
    navController: NavController,
    onBottomNavOptionSelected: OnBottomNavOptionSelected,
    favouritesCount: Int
) {
    Box {
        NavigationBar(
            modifier = Modifier.height(Dimens.BottomNavigationBar.height),
            containerColor = AppColors.colorPrimary
        ) {
            val navBackStackEntry by navController.currentBackStackEntryAsState()
            val currentDestination = navBackStackEntry?.destination
            BottomTab(
                screen = MainScreenTabs.Vehicles,
                currentDestination = currentDestination,
                onBottomNavOptionsSelected = onBottomNavOptionSelected,
                navController = navController
            )
            BottomTab(
                screen = MainScreenTabs.SellNow,
                currentDestination = currentDestination,
                onBottomNavOptionsSelected = onBottomNavOptionSelected,
                navController = navController,
                badgeCount = favouritesCount
            )
        }
    }
}

@Composable
private fun RowScope.BottomTab(
    screen: MainScreenTabs,
    currentDestination: NavDestination?,
    onBottomNavOptionsSelected: OnBottomNavOptionSelected,
    navController: NavController,
    badgeCount: Int = 0
) {
    val focusManager = LocalFocusManager.current
    NavigationBarItem(
        icon = {
            BadgedBox(
                badge = {
                    if (badgeCount > 0) {
                        Badge {
                            Text(badgeCount.toString())
                        }
                    }
                }
            ) {
                Icon(
                    painter = painterResource(id = screen.iconId),
                    contentDescription = null
                )
            }

        },
        label = {
            Text(
                text = stringResource(screen.resourceId),
                style = TextStyle(),
                maxLines = 1
            )
        },
        selected = currentDestination?.hierarchy?.any {
            it.route == RouteBuilder.buildRoute(screen.route)
        } == true,
        onClick = {
            focusManager.clearFocus()
            onBottomNavOptionsSelected(screen)
            val uri = RouteBuilder.buildDestination(
                path = screen.route
            )
            if (navController.currentDestination?.route == uri) {
                navController.popBackStack()
            }
            navController.navigate(uri)
        },
        colors = NavigationBarItemDefaults.colors(
            selectedIconColor = Color.White,
            selectedTextColor = Color.White,
            unselectedIconColor = AppColors.colorBorder,
            unselectedTextColor = AppColors.colorBorder,
            indicatorColor = AppColors.colorPrimary
        )
    )
}
