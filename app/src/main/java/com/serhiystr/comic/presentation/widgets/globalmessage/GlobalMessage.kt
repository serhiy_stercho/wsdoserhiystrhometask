package com.serhiystr.comic.presentation.widgets.globalmessage

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.with
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import com.lesautomotive.bidacar.theme.contentMedium16
import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens
import kotlinx.coroutines.delay

@Composable
fun GlobalMessage(
    content: @Composable () -> Unit
) {
    val state = remember { GlobalMessageState() }

    CompositionLocalProvider(LocalGlobalMessage provides state) {
        Box(
            Modifier.fillMaxSize()
        ) {
            content()
            val stateMessage = state.message
            AnimatedContent(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(
                        top = Dimens.margin4x,
                        start = Dimens.margin2x,
                        end = Dimens.margin2x
                    ),
                targetState = stateMessage,
                transitionSpec = {
                    (
                        slideInVertically() + fadeIn()
                            with slideOutHorizontally { it / 2 } + fadeOut()
                        ).using(SizeTransform(false))
                }
            ) { state ->
                if (state == null) {
                    Box {}
                } else {
                    Box(contentAlignment = Alignment.TopCenter) {
                        MessageContent(
                            removable = false,
                            title = state.title,
                            body = state.body,
                            backgroundColor = state.style.backgroundColor,
                            remove = {}
                        )
                    }
                }
            }
        }
    }

    LaunchedEffect(key1 = state.message) {
        val message = state.message ?: return@LaunchedEffect
        delay(message.duration)
        state.hide()
    }
}

@Composable
private fun MessageContent(
    modifier: Modifier = Modifier,
    title: String,
    body: String,
    backgroundColor: Color,
    removable: Boolean,
    remove: VoidCallback
) {
    Card(
        modifier
            .fillMaxWidth()
            .wrapContentHeight(),
        colors = CardDefaults.cardColors(
            containerColor = backgroundColor
        )

    ) {
        Row(
            Modifier.padding(Dimens.margin2x),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(Modifier.weight(1F)) {
                if (title.isNotEmpty()) {
                    Text(
                        text = title,
                        style = MaterialTheme.typography.contentMedium16,
                        color = MaterialTheme.colorScheme.onSurface
                    )
                }
                if (body.isNotEmpty()) {
                    Text(
                        text = body,
                        style = MaterialTheme.typography.contentMedium16,
                        color = MaterialTheme.colorScheme.onSurface
                    )
                }
            }
        }
    }
}

@Composable
fun ListenGlobalMessage(
    message: GlobalMessage?,
    catch: VoidCallback
) {
    val context = LocalContext.current
    val messageState = LocalGlobalMessage.current
    LaunchedEffect(message) {
        message ?: return@LaunchedEffect
        catch()
        message.show(messageState, context)
    }
}
