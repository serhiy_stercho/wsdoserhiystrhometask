package com.serhiystr.comic.presentation.screens.comics

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lesautomotive.bidacar.utils.ext.stateIn
import com.serhiystr.comic.data.convertes.toDomainModel
import com.serhiystr.comic.data.convertes.toUIModel
import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.comic.domain.usecases.local.AddToFavourites
import com.serhiystr.comic.domain.usecases.local.RemoveFavourite
import com.serhiystr.comic.domain.usecases.remote.GetComicsForYear
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toImmutableList
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import okhttp3.internal.immutableListOf

class ComicsViewModel(
    private val getComicsForYear: GetComicsForYear,
    private val addFavourite: AddToFavourites,
    private val removeFavourite: RemoveFavourite
) : ViewModel() {

    private var allComics = arrayListOf<ComicsUIModel>()

    private val _comics = MutableStateFlow<ImmutableList<ComicsUIModel>>(persistentListOf())
    val comics: StateFlow<ImmutableList<ComicsUIModel>> = _comics

    private val _searchQuery = MutableStateFlow<String>("")
    val searchQuery: StateFlow<String> = _searchQuery

    init {
        getComicsListRemote()
    }

    private fun getComicsListRemote() {
        viewModelScope.launch {
            val comics = getComicsForYear(COMICS_YEAR)
            if (comics.isFailure) return@launch
            allComics.clear()
            allComics.addAll(comics.getOrThrow().map { it.toUIModel() }.toPersistentList())
            _comics.emit(comics.getOrThrow().map { it.toUIModel() }.toPersistentList())
        }
    }

    fun searchComics(query: String) {
        _searchQuery.value = query
        if (query.isEmpty()) {
            _comics.tryEmit(persistentListOf<ComicsUIModel>().addAll(allComics))
        } else {
            val filteredComics = allComics.filter { comics ->
                comics.title.contains(query, ignoreCase = true) ||
                        comics.description.contains(query, ignoreCase = true)
            }
            _comics.tryEmit(persistentListOf<ComicsUIModel>().addAll(filteredComics))
        }
    }

    fun addToFavourites(comicsUIModel: ComicsUIModel) {
        viewModelScope.launch {
            addFavourite(comicsUIModel.toDomainModel())

            val comicIndex = allComics.indexOfFirst { it.id == comicsUIModel.id }
            if (comicIndex != -1) {
                allComics.set(comicIndex, comicsUIModel.copy(isFavourite = true))
                _comics.tryEmit(persistentListOf<ComicsUIModel>().addAll(allComics))
            }
        }
    }

    fun removeFromFavourites(comicsUIModel: ComicsUIModel) {
        viewModelScope.launch {
            removeFavourite(comicsUIModel.id)

            val comicIndex = allComics.indexOfFirst { it.id == comicsUIModel.id }
            if (comicIndex != -1) {
                allComics.set(comicIndex, comicsUIModel.copy(isFavourite = false))
                _comics.tryEmit(persistentListOf<ComicsUIModel>().addAll(allComics))
            }
        }
    }

    companion object {
        //The Year is hardcoded for test project
        private val COMICS_YEAR = 2018
    }

}