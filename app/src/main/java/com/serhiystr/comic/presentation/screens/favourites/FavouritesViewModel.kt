package com.serhiystr.comic.presentation.screens.favourites

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lesautomotive.bidacar.utils.ext.stateIn
import com.serhiystr.comic.data.convertes.toUIModel
import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.comic.domain.usecases.local.GetAllFavourites
import com.serhiystr.comic.domain.usecases.local.GetAllFavouritesAsFlow
import com.serhiystr.comic.domain.usecases.local.RemoveFavourite
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class FavouritesViewModel(
    private val getAllFavouritesAsFlow: GetAllFavouritesAsFlow,
    private val removeFavourite: RemoveFavourite,
) : ViewModel() {

    private val _searchQuery = MutableStateFlow<String>("")
    val searchQuery: StateFlow<String> = _searchQuery

    val comics: StateFlow<ImmutableList<ComicsUIModel>> = getAllFavouritesAsFlow()
        .map { comics -> comics.map { it.toUIModel() }.toPersistentList()}
        .stateIn(viewModelScope, persistentListOf())

    private val _comicsSearch = MutableStateFlow<ImmutableList<ComicsUIModel>>(persistentListOf())
    val comicsSearch: StateFlow<ImmutableList<ComicsUIModel>> = _comicsSearch


    fun removeFromFavourites(comicId: Int) {
        viewModelScope.launch {
            removeFavourite(comicId)
            removeFromSearchDataSet(comicId)
        }
    }

    private fun removeFromSearchDataSet(comicId: Int) {
        val searchDataSet = _comicsSearch.value.toMutableList()
        searchDataSet.removeIf { it.id == comicId }
        _comicsSearch.value = persistentListOf<ComicsUIModel>().addAll(searchDataSet)
    }


    fun searchComics(query: String?) {
        _searchQuery.value = query ?: ""
        if (query.isNullOrEmpty()) {
            _comicsSearch.tryEmit(persistentListOf<ComicsUIModel>().addAll(comics.value))
        } else {
            val filteredComics = comics.value.filter { comics ->
                comics.title.contains(query, ignoreCase = true) ||
                        comics.description.contains(query, ignoreCase = true)
            }
            _comicsSearch.tryEmit(persistentListOf<ComicsUIModel>().addAll(filteredComics))
        }
    }

}
