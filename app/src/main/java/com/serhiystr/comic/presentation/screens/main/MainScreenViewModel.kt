package com.serhiystr.comic.presentation.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lesautomotive.bidacar.utils.ext.stateIn
import com.serhiystr.comic.data.convertes.toUIModel
import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.comic.domain.usecases.local.GetAllFavourites
import com.serhiystr.comic.domain.usecases.local.GetAllFavouritesAsFlow
import kotlinx.collections.immutable.ImmutableList
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.toPersistentList
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map

class MainScreenViewModel(
    private val getAllFavouritesAsFlow: GetAllFavouritesAsFlow
) : ViewModel() {

    val favouritesSize: StateFlow<Int> = getAllFavouritesAsFlow()
        .map { it.size}
        .stateIn(viewModelScope, 0)
}
