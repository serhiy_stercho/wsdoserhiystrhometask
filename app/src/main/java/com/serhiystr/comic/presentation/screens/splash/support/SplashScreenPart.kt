package com.serhiystr.comic.presentation.screens.splash.support

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavController
import com.serhiystr.comic.presentation.screens.main.support.navigateToMain
import com.serhiystr.comic.utils.VoidCallback

object SplashScreenScope

sealed class SplashScreenNavigateTo {
    object Login : SplashScreenNavigateTo()
    object Main : SplashScreenNavigateTo()
}

@Composable
fun SplashScreenScope.ListenNavigation(
    navigateTo: SplashScreenNavigateTo?,
    navController: NavController,
    catched: VoidCallback
) {
    LaunchedEffect(navigateTo) {
        val destination = navigateTo ?: return@LaunchedEffect
        catched()
        navController.navigateToMain()
    }
}
