package com.serhiystr.comic.presentation.widgets.showOn

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.core.LinearOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.scaleIn
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import com.serhiystr.comic.utils.constants.Durations
import kotlinx.coroutines.delay
import kotlin.time.Duration

@Composable
fun ShowOnStart(
    modifier: Modifier = Modifier,
    enter: EnterTransition,
    isAlwaysTakePlace: Boolean = false,
    enterDuration: Duration? = null,
    content: @Composable () -> Unit
) {
    var showEnterAnimation by rememberSaveable {
        mutableStateOf(false)
    }
    var placeholderVisible by rememberSaveable() {
        mutableStateOf(true)
    }
    if (isAlwaysTakePlace) {
        Box(
            modifier = modifier
        ) {
            AnimatedVisibility(
                visible = showEnterAnimation,
                enter = enter
            ) {
                content()
            }
            if (placeholderVisible) {
                Box(
                    Modifier.alpha(0F)
                ) {
                    content()
                }
            }
        }
    } else {
        AnimatedVisibility(
            modifier = modifier,
            visible = showEnterAnimation,
            enter = enter
        ) {
            content()
        }
    }
    LaunchedEffect(key1 = true) {
        showEnterAnimation = true
        enterDuration?.let {
            delay(it)
            placeholderVisible = false
        }
    }
}

@Composable
fun ShowFromTop(
    modifier: Modifier = Modifier,
    duration: Duration = Durations.Animation.animationEnterScreenObject,
    delay: Duration = Duration.ZERO,
    isAlwaysTakePlace: Boolean = false,
    initialAlpha: Float = 0.3F,
    content: @Composable () -> Unit
) {
    val density = LocalDensity.current
    ShowOnStart(
        modifier = modifier,
        enter = slideInVertically(
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing
            )
        ) {
            with(density) { -80.dp.roundToPx() }
        } + fadeIn(
            initialAlpha = initialAlpha,
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ),
        enterDuration = duration + delay,
        isAlwaysTakePlace = isAlwaysTakePlace
    ) {
        content()
    }
}

@Composable
fun ShowFromBottom(
    modifier: Modifier = Modifier,
    duration: Duration = Durations.Animation.animationEnterScreenObject,
    delay: Duration = Duration.ZERO,
    initialAlpha: Float = 0.3F,
    isAlwaysTakePlace: Boolean = false,
    content: @Composable () -> Unit
) {
    val density = LocalDensity.current
    ShowOnStart(
        modifier = modifier,
        enter = slideInVertically(
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ) {
            with(density) { +80.dp.roundToPx() }
        } + fadeIn(
            initialAlpha = initialAlpha,
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ),
        enterDuration = duration + delay,
        isAlwaysTakePlace = isAlwaysTakePlace
    ) {
        content()
    }
}

@Composable
fun ShowFromLeft(
    modifier: Modifier = Modifier,
    duration: Duration = Durations.Animation.animationEnterScreenObject,
    delay: Duration = Duration.ZERO,
    isAlwaysTakePlace: Boolean = false,
    initialAlpha: Float = 0.3F,
    content: @Composable () -> Unit
) {
    val density = LocalDensity.current
    ShowOnStart(
        modifier = modifier,
        enter = slideInHorizontally(
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ) {
            with(density) { -80.dp.roundToPx() }
        } + fadeIn(
            initialAlpha = initialAlpha,
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ),
        enterDuration = duration + delay,
        isAlwaysTakePlace = isAlwaysTakePlace
    ) {
        content()
    }
}

@Composable
fun ShowFromRight(
    modifier: Modifier = Modifier,
    duration: Duration = Durations.Animation.animationEnterScreenObject,
    delay: Duration = Duration.ZERO,
    isAlwaysTakePlace: Boolean = false,
    initialAlpha: Float = 0.3F,
    content: @Composable () -> Unit
) {
    val density = LocalDensity.current
    ShowOnStart(
        modifier = modifier,
        enter = slideInHorizontally(
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ) {
            with(density) { +80.dp.roundToPx() }
        } + fadeIn(
            initialAlpha = initialAlpha,
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ),
        enterDuration = duration + delay,
        isAlwaysTakePlace = isAlwaysTakePlace
    ) {
        content()
    }
}

@Composable
fun ShowFromCenter(
    modifier: Modifier = Modifier,
    duration: Duration = Durations.Animation.animationEnterScreenObject,
    delay: Duration = Duration.ZERO,
    isAlwaysTakePlace: Boolean = false,
    initialAlpha: Float = 0.3F,
    content: @Composable () -> Unit
) {
    ShowOnStart(
        modifier,
        enter = scaleIn(
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            ),
            initialScale = 0.8F
        ) + fadeIn(
            initialAlpha = initialAlpha,
            animationSpec = tween(
                durationMillis = duration.inWholeMilliseconds.toInt(),
                easing = LinearOutSlowInEasing,
                delayMillis = delay.inWholeMilliseconds.toInt()
            )
        ),
        enterDuration = duration + delay,
        isAlwaysTakePlace = isAlwaysTakePlace
    ) {
        content()
    }
}
