package com.serhiystr.comic.presentation.navigation

import android.net.Uri

object RouteBuilder {

    fun buildRoute(
        path: String,
        vararg queries: String
    ): String {
        return Uri.Builder()
            .appendEncodedPath(path)
            .apply {
                queries.forEach {
                    appendQueryParameter(it, "{$it}")
                }
            }
            .build().toString()
    }

    fun buildDestination(
        path: String,
        vararg queries: Pair<String, String?>
    ): String {
        return Uri.Builder()
            .appendEncodedPath(path)
            .apply {
                queries.forEach {
                    if (it.second != null) {
                        appendQueryParameter(it.first, it.second)
                    }
                }
            }
            .build().toString()
    }
}
