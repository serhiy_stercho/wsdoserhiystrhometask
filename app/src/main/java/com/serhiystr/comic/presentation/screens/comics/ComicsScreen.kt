package com.serhiystr.comic.presentation.screens.comics


import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavController
import com.lesautomotive.bidacar.theme.titleBold18
import com.serhiystr.comic.utils.constants.Dimens
import com.serhiystr.comic.data.uimodels.ComicsUIModel
import com.serhiystr.comic.presentation.screens.favourites.ui.SearchTextField
import com.serhiystr.comic.presentation.widgets.comiscitem.ComicsItem
import com.serhiystr.comic.presentation.widgets.comiscitem.OnComicFavouriteClicked
import com.serhiystr.comics.R
import kotlinx.collections.immutable.ImmutableList
import org.koin.androidx.compose.getViewModel


@Composable
fun ComicsScreen(
    navController: NavController,
    viewModel: ComicsViewModel = getViewModel()
) {

    val comics by viewModel.comics.collectAsState()
    val searchQuery by viewModel.searchQuery.collectAsState()

    val lazyListState = rememberLazyListState()

    ComicsScreenContent(
        items = comics,
        searchQuery = searchQuery,
        onSearchQueryChanged = {
            viewModel.searchComics(it)
        },
        lazyListState = lazyListState,
        onComicFavouriteClicked = { comics ->
            if (comics.isFavourite) {
                viewModel.removeFromFavourites(comics)
            } else {
                viewModel.addToFavourites(comics)
            }
        }
    )
}

@Composable
private fun ComicsScreenContent(
    items: ImmutableList<ComicsUIModel>,
    searchQuery: String,
    onSearchQueryChanged: (String) -> Unit,
    lazyListState: LazyListState,
    onComicFavouriteClicked: OnComicFavouriteClicked
) {
    Scaffold(
        Modifier
            .fillMaxSize()
            .statusBarsPadding()
    ) {
        Column(Modifier.fillMaxSize()) {
            SearchTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(Dimens.margin2x),
                value = searchQuery,
                onTextChanged = onSearchQueryChanged,
                label = "Search comics"
            )
            when {
                items.isEmpty() && searchQuery.isEmpty() -> CircleLoader()
                items.isEmpty() && searchQuery.isNotEmpty() -> EmptyList()
                else -> ComicsList(
                    items = items,
                    lazyListState = lazyListState,
                    onComicFavouriteClicked = onComicFavouriteClicked
                )
            }
        }
    }
}


@Composable
private fun ComicsList(
    items: ImmutableList<ComicsUIModel>,
    lazyListState: LazyListState,
    onComicFavouriteClicked: OnComicFavouriteClicked
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        state = lazyListState,
        verticalArrangement = Arrangement.spacedBy(Dimens.margin6dp),
        contentPadding = PaddingValues(horizontal = Dimens.margin6dp)
    ) {
        items(
            items = items,
            key = { it.id }
        ) { _comics ->
            ComicsItem(
                comics = _comics,
                onComicFavouriteClicked = onComicFavouriteClicked
            )
        }
    }
}

@Composable
private fun EmptyList() {
    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            Modifier.align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_list),
                contentDescription = null,
                modifier = Modifier.size(Dimens.FavouritesScreen.emptyStateIconSize)
            )
            Spacer(modifier = Modifier.height(Dimens.margin2x))
            Text(
                text = "Couldn't find any comics",
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleBold18,
                color = Color.Black
            )
        }
    }
}

@Composable
private fun CircleLoader() {
    Box(Modifier.fillMaxSize()) {
        CircularProgressIndicator(Modifier
            .size(Dimens.ComicsScreen.loaderIndicatorSize)
            .align(Alignment.Center)
        )
    }
}




