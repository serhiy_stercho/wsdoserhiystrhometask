package com.serhiystr.comic.presentation.widgets.navigationtoolbar

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import com.lesautomotive.bidacar.theme.AppColors
import com.lesautomotive.bidacar.theme.contentRegular16
import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens
import com.lesautomotive.bidacar.utils.ext.conditional
import com.serhiystr.comics.R

@Composable
fun NavigationToolbar(
    title: String,
    nextActionLabel: String? = null,
    onNextActionClicked: VoidCallback = {},
    nextButtonEnabled: Boolean = true,
    showBackNavigation: Boolean = true,
    onBackClicked: VoidCallback = {},
    backIconRes: Int = R.drawable.ic_arrow_back,
    backNavigationLabel: String? = null
) {
    Box(
        Modifier
            .fillMaxWidth()
            .background(AppColors.colorPrimary)
            .height(Dimens.BaseToolbar.height)
    ) {
        Row(
            Modifier
                .fillMaxHeight()
                .align(Alignment.CenterStart)
                .clickable {
                    onBackClicked()
                },
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(Modifier.width(Dimens.marginAndHalf))
            Icon(
                painter = painterResource(backIconRes),
                contentDescription = null,
                tint = Color.Unspecified
            )
            if (backNavigationLabel != null) {
                Spacer(Modifier.width(Dimens.margin6dp))
                Text(
                    text = backNavigationLabel,
                    modifier = Modifier
                        .align(Alignment.CenterVertically),
                    style = MaterialTheme.typography.contentRegular16,
                    color = Color.White,
                    textAlign = TextAlign.Center
                )
            }
        }
        Text(
            text = title,
            modifier = Modifier.align(Alignment.Center),
            style = TextStyle(
                fontSize = Dimens.BaseToolbar.titleSize,
                fontWeight = FontWeight.W700
            ),
            color = Color.White
        )
        if (nextActionLabel != null) {
            val textColor = remember(nextButtonEnabled) {
                if (nextButtonEnabled) Color.White else AppColors.colorBorder
            }
            Row(
                Modifier
                    .align(Alignment.CenterEnd)
                    .conditional(nextButtonEnabled) {
                        clickable {
                            onNextActionClicked()
                        }
                    },
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = nextActionLabel,
                    style = MaterialTheme.typography.contentRegular16,
                    color = textColor
                )
                Spacer(Modifier.width(Dimens.marginAndHalf))
            }
        }
    }
}
