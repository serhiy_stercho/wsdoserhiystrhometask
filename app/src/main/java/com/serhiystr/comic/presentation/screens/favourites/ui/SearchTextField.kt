package com.serhiystr.comic.presentation.screens.favourites.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lesautomotive.bidacar.theme.AppColors
import com.serhiystr.comic.utils.OnTextChanged
import com.serhiystr.comic.utils.constants.Dimens


@Composable
fun SearchTextField(
    modifier: Modifier = Modifier,
    value: String,
    onTextChanged: OnTextChanged,
    label: String? = "",
    labelColor: Color = AppColors.Login.textFieldBorder,
    focusRequester: FocusRequester = remember { FocusRequester() }
) {
    Surface(
        modifier.focusRequester(focusRequester)
            .height(Dimens.SellNow.vinTextFieldHeight),
        border = BorderStroke(
            2.dp,
            AppColors.Login.textFieldBorder
        ),
        shape = RoundedCornerShape(12.dp),
        color = Color.Transparent
    ) {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(
                    start = 4.dp,
                    end = 4.dp,
                    top = 4.dp,
                    bottom = 4.dp
                ),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(Modifier.width(Dimens.margin2x))
            BasicTextField(
                modifier = Modifier
                    .weight(1F),
                value = value,
                textStyle = TextStyle(
                    fontSize = 18.sp,
                    textAlign = TextAlign.Start,
                    color = Color.Black
                ),
                singleLine = true,
                cursorBrush = SolidColor(MaterialTheme.colorScheme.primary),
                onValueChange = onTextChanged,
                decorationBox = { inner ->
                    if (label.isNullOrEmpty().not() && value.isEmpty()) {
                        Text(
                            text = label!!,
                            style = TextStyle(
                                fontSize = 18.sp,
                                textAlign = TextAlign.Start,
                                color = labelColor
                            ),
                            color = labelColor
                        )
                    }
                    inner()
                }
            )
            Spacer(Modifier.width(Dimens.margin2x))
        }
    }
}