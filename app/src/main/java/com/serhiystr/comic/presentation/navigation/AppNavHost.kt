package com.serhiystr.comic.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.google.accompanist.navigation.animation.AnimatedNavHost

@Composable
fun AppNavHost(
    modifier: Modifier = Modifier,
    navController: NavHostController,
    startDestination: String,
    animated: Boolean = true,
    builder: NavGraphBuilder.() -> Unit,
) {
    if (animated) {
        AnimatedNavHost(
            modifier = modifier,
            navController = navController,
            startDestination = startDestination,
            builder = builder,
        )
    } else {
        NavHost(
            modifier = modifier,
            navController = navController,
            startDestination = startDestination,
            builder = builder,
        )
    }
}
