package com.serhiystr.comic.presentation.screens.main.support

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.serhiystr.comic.presentation.screens.main.comicstab.ComicsTab
import com.serhiystr.comic.presentation.screens.main.favourites.FavouritesTab
import com.serhiystr.comics.R

object MainScreenScope

sealed class MainScreenTabs(
    val route: String,
    @StringRes val resourceId: Int,
    @DrawableRes val iconId: Int
) {

    object Vehicles : MainScreenTabs(ComicsTab.route, R.string.main_scr_tab_comics, R.drawable.ic_list)
    object SellNow : MainScreenTabs(FavouritesTab.route, R.string.main_scr_tab_favourites, R.drawable.ic_favorite)

}
