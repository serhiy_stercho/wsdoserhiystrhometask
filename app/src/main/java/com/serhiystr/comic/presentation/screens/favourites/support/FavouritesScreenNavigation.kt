package com.serhiystr.comic.presentation.screens.favourites.support

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import com.serhiystr.comic.presentation.screens.favourites.FavouritesScreen
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.animatedComposable


object FavouritesScreen {
    const val route = "favourites"
}

fun NavGraphBuilder.favouritesComposable(
    navController: NavController
) {
    animatedComposable(
        route = RouteBuilder.buildRoute(
            path = FavouritesScreen.route
        )
    ) {
        FavouritesScreen(
            navController = navController
        )
    }
}
