package com.serhiystr.comic.presentation.app.containers

import androidx.compose.runtime.Composable
import com.serhiystr.comic.presentation.widgets.globalloader.GlobalLoader
import com.serhiystr.comic.presentation.widgets.globalloader.GlobalLoaderState
import com.serhiystr.comic.utils.VoidCallbackComposable

@Composable
fun AppDialogs(
    loaderState: GlobalLoaderState,
    content: VoidCallbackComposable
) {
    GlobalLoader(state = loaderState) {
        content()
    }
}
