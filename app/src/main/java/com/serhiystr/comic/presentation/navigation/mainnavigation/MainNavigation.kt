package com.serhiystr.comic.presentation.navigation.mainnavigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.serhiystr.comic.presentation.navigation.AppNavHost
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.screens.main.comicstab.ComicsTab
import com.serhiystr.comic.presentation.screens.main.comicstab.comicsTabComposable
import com.serhiystr.comic.presentation.screens.main.favourites.favouritesTabComposable


@Composable
fun MainNavigation(
    modifier: Modifier = Modifier,
    navController: NavHostController
) {
    AppNavHost(
        modifier = modifier,
        navController = navController,
        startDestination = RouteBuilder.buildRoute(ComicsTab.route),
        animated = false
    ) {
        comicsTabComposable()
        favouritesTabComposable()
    }
}
