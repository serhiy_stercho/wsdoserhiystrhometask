package com.serhiystr.comic.presentation.screens.comics.support

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder

import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.animatedComposable
import com.serhiystr.comic.presentation.screens.comics.ComicsScreen

object ComicsScreen {
    const val route = "comics"
}

fun NavGraphBuilder.comicsScreenComposable(
    navController: NavController
) {
    animatedComposable(
        route = RouteBuilder.buildRoute(
            path = ComicsScreen.route
        )
    ) {
        ComicsScreen(
            navController = navController
        )
    }
}
