package com.serhiystr.comic.presentation.screens.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.serhiystr.comic.presentation.screens.splash.support.SplashScreenNavigateTo
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SplashViewModel(

) : ViewModel() {

    private val _navigateTo = MutableStateFlow<SplashScreenNavigateTo?>(null)
    val navigateTo: StateFlow<SplashScreenNavigateTo?> = _navigateTo

    init {
        checkData()
    }

    private fun checkData() {
        viewModelScope.launch {
            delay(1500)
            _navigateTo.emit(SplashScreenNavigateTo.Main)
        }
    }

    fun onNavigateCatched() {
        _navigateTo.value = null
    }
}