package com.serhiystr.comic.presentation.widgets.globalmessage

import android.content.Context
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.serhiystr.comic.utils.constants.Durations
import kotlin.time.Duration

class GlobalMessageState {
    internal var message by mutableStateOf<Message?>(null)

    fun show(
        body: String,
        title: String = "",
        duration: Duration = Durations.globalMessage,
        style: GlobalMessageStyle = GlobalMessageStyle.ERROR
    ) {
        this.message = Message(
            title = title,
            body = body,
            duration = duration,
            style = style
        )
    }

    fun hide() {
        message = null
    }

    internal class Message(
        val title: String,
        val body: String,
        val duration: Duration,
        val style: GlobalMessageStyle
    )
}

interface GlobalMessage {
    fun show(state: GlobalMessageState, context: Context)
}

interface GlobalMessageTextError : GlobalMessage {
    val text: String
    override fun show(state: GlobalMessageState, context: Context) {
        state.show(text)
    }
}

interface GlobalMessageTextSuccess : GlobalMessage {
    val text: String
    override fun show(state: GlobalMessageState, context: Context) {
        state.show(
            text,
            style = GlobalMessageStyle.SUCCESS
        )
    }
}

val LocalGlobalMessage = compositionLocalOf { GlobalMessageState() }
