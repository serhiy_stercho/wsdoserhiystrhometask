package com.serhiystr.comic.presentation.screens.splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.serhiystr.comic.presentation.screens.splash.support.ListenNavigation
import com.serhiystr.comic.presentation.screens.splash.support.SplashScreenScope
import com.serhiystr.comics.R
import org.koin.androidx.compose.getViewModel

@Composable
fun SplashScreen(
    navController: NavController,
    viewModel: SplashViewModel = getViewModel()
) {
    val navigateTo by viewModel.navigateTo.collectAsState()
    SplashScreenContent()
    SplashScreenScope.ListenNavigation(navigateTo, navController) {
        viewModel.onNavigateCatched()
    }
}

@Composable
fun SplashScreenContent() {
    Scaffold(Modifier.fillMaxSize()) {
        Column(
            Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Spacer(Modifier.height(124.dp))
            Image(
                painter = painterResource(R.drawable.ic_marvel_logo),
                modifier = Modifier.size(100.dp),
                contentDescription = null
            )

        }
    }
}
