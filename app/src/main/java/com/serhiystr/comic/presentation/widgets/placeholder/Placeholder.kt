package com.serhiystr.comic.presentation.widgets.placeholder

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.with
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource

import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens
import com.serhiystr.comic.utils.constants.Durations
import com.serhiystr.comics.R

@Composable
fun <S> Placeholder(
    modifier: Modifier = Modifier,
    targetState: S,
    enterTransition: EnterTransition = fadeIn(
        animationSpec = tween(
            Durations.Animation.Placeholder.enter.inWholeMilliseconds.toInt(),
            Durations.Animation.Placeholder.enterDelay.inWholeMilliseconds.toInt()
        )
    ),
    content: @Composable (S) -> Unit
) {
    AnimatedContent(
        targetState = targetState,
        modifier = modifier,
        contentAlignment = Alignment.Center,
        transitionSpec = {
            enterTransition with fadeOut(
                animationSpec = tween(Durations.Animation.Placeholder.exit)
            )
        }
    ) { currentState ->
        content(currentState)
    }
}

@Composable
fun ErrorPlaceHolder(
    message: String,
    retry: VoidCallback? = null,
    modifier: Modifier = Modifier
) {
    Column(
        modifier.verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(message)
        Spacer(Modifier.height(Dimens.margin2x))
        if (retry != null) {
            Spacer(Modifier.height(Dimens.margin2x))
            OutlinedButton(
                onClick = retry
            ) {
                Text(
                    text = stringResource(R.string.general_retry)
                )
            }
        }
    }
}

@Composable
fun LoadingPlaceHolder(
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colorScheme.primary
) {
    Box(
        modifier.verticalScroll(rememberScrollState()),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            color = color
        )
    }
}

// @Composable
// fun Throwable.getPlaceHolderText(): String {
//    return when (this) {
//        is InternetConnectionException -> stringResource(R.string.error_internet_connection)
//        is TimeoutException -> stringResource(R.string.error_timeout)
//        is HttpException -> message
//        else -> stringResource(R.string.error_unknown)
//    }
// }
