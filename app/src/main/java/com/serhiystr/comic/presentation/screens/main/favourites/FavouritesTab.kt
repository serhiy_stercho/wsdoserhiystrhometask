package com.serhiystr.comic.presentation.screens.main.favourites

import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.favouritesnavigation.FavouritesNavigation

object FavouritesTab {
    const val route = "favouritesTab"
}

@Composable
fun FavouritesTab() {
    FavouritesNavigation(navController = rememberAnimatedNavController())
}

fun NavGraphBuilder.favouritesTabComposable() {
    composable(
        RouteBuilder.buildRoute(FavouritesTab.route)
    ) {
        FavouritesTab()
    }
}
