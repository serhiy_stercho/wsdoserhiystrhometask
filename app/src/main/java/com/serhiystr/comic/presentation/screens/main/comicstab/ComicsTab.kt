package com.serhiystr.comic.presentation.screens.main.comicstab

import androidx.compose.runtime.Composable
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.comicsnavigation.ComicsNavigation


object ComicsTab {
    const val route = "comicsTab"
}

@Composable
fun ComicsTab() {
    ComicsNavigation(navController = rememberAnimatedNavController())
}

fun NavGraphBuilder.comicsTabComposable() {
    composable(
        RouteBuilder.buildRoute(ComicsTab.route)
    ) {
        ComicsTab()
    }
}
