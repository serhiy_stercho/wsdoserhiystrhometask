package com.serhiystr.comic.presentation.navigation.favouritesnavigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import com.serhiystr.comic.presentation.screens.favourites.support.FavouritesScreen
import com.serhiystr.comic.presentation.navigation.AppNavHost
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.screens.favourites.support.favouritesComposable


@Composable
fun FavouritesNavigation(
    modifier: Modifier = Modifier,
    navController: NavHostController
) {
    AppNavHost(
        modifier = modifier,
        navController = navController,
        startDestination = RouteBuilder.buildRoute(FavouritesScreen.route)
    ) {
        favouritesComposable(navController)
    }
}