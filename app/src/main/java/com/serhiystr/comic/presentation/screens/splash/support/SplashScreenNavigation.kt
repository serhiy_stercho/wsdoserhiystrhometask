package com.serhiystr.comic.presentation.screens.splash.support

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import com.serhiystr.comic.presentation.navigation.RouteBuilder
import com.serhiystr.comic.presentation.navigation.animatedComposable
import com.serhiystr.comic.presentation.screens.splash.SplashScreen

object SplashScreen {
    const val path = "splashScreen"
    val route = RouteBuilder.buildRoute(
        path = path
    )
}

fun NavGraphBuilder.splashScreenComposable(
    navController: NavController
) {
    animatedComposable(
        SplashScreen.route
    ) { entry ->
        SplashScreen(
            navController
        )
    }
}
