package com.serhiystr.comic.presentation.widgets.image

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.serhiystr.comic.presentation.widgets.fadeanimatedvisibility.FadeAnimatedVisibility
import com.lesautomotive.bidacar.theme.AppColors
import com.serhiystr.comic.utils.constants.Dimens
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage

@Composable
fun RemoteImage(
    modifier: Modifier = Modifier,
    url: String?,
    @DrawableRes placeHolder: Int? = null,
    @DrawableRes error: Int? = null,
    contentScale: ContentScale = ContentScale.Crop,
    alignment: Alignment = Alignment.Center,
    showLoader: Boolean = false,
    placeHolderColor: Color = AppColors.colorBorder,
) {
    GlideImage(
        modifier = modifier,
        imageOptions = ImageOptions(
            alignment = alignment,
            contentScale = contentScale,
        ),
        loading = {
            if (placeHolder != null) {
                LocalImage(
                    modifier = Modifier.fillMaxSize(),
                    id = placeHolder,
                    contentScale = ContentScale.Crop,
                )
            } else {
                Box(Modifier.fillMaxSize().background(placeHolderColor))
            }
            FadeAnimatedVisibility(
                visible = showLoader,
                modifier = Modifier.align(Alignment.Center),
            ) {
                CircularProgressIndicator(
                    modifier = Modifier.size(Dimens.Loader.imageLoader),
                    strokeWidth = 3.dp,
                )
            }
        },
        previewPlaceholder = placeHolder ?: 0,
        requestBuilder = {
            Glide
                .with(LocalContext.current)
                .asBitmap()
                .load(url)
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
        },
        imageModel = { url },
        failure = error?.let { id ->
            {
                LocalImage(
                    modifier = Modifier.fillMaxSize(),
                    id = id,
                    contentScale = ContentScale.Crop,
                )
            }
        },
    )
}