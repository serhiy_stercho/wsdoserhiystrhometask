package com.lesautomotive.bidacar.presentation.widgets

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.style.TextAlign
import com.lesautomotive.bidacar.theme.AppColors
import com.lesautomotive.bidacar.theme.buttonRegular14
import com.serhiystr.comic.utils.VoidCallback
import com.serhiystr.comic.utils.constants.Dimens

@Composable
fun ColorActionButton(
    modifier: Modifier = Modifier,
    text: String,
    onPressed: VoidCallback,
    color: Color = AppColors.colorAcceptBid,
    shape: Shape = RoundedCornerShape(Dimens.ColorActionButton.corners),
    paddings: PaddingValues = PaddingValues(
        horizontal = Dimens.margin2x,
        vertical = Dimens.margin
    )
) {
    Box(
        Modifier
            .clip(shape)
            .background(color = color, shape = shape)
            .clickable { onPressed() }
            .padding(paddings)
    ) {
        Text(
            text = text,
            modifier = Modifier.align(Alignment.Center),
            style = MaterialTheme.typography.buttonRegular14,
            color = Color.White,
            textAlign = TextAlign.Center
        )
    }
}
