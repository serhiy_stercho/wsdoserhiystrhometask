buildscript {
    dependencies {
        classpath("com.google.gms:google-services:4.3.15")
        classpath("com.google.firebase:firebase-crashlytics-gradle:2.9.5")
    }
}

plugins {
    val kotlinVersion = "1.8.20"
    id("com.android.application") version "8.0.0" apply false
    id("com.android.library") version "8.0.0" apply false
    id("org.jetbrains.kotlin.android") version kotlinVersion apply false
    kotlin("plugin.serialization") version kotlinVersion
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}